define(function (require, exports, module) {

    var Backbone = require('backbone');
    var Event = require('models/event');

    var Events = Backbone.Collection.extend({
        model: Event
    });

    return Events;
});
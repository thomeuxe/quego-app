define(function (require, exports, module) {

    var Backbone = require('backbone');
    var User = require('models/user');

    var event_bus = require('event_bus');
    var ServerListener = require('ServerListener');

    var Users = Backbone.Collection.extend({
        model: User,
        url: function () {
            return ServerListener.serverURL + '/users'
        },

        initialize: function (options) {
            options = options || {};
            this.userSpecificUrl = options.userSpecificUrl || "";
        },

        getChallengers: function (amount) {
            return this.models.filter(function (user) {
                return user.id != ServerListener.socketID;
            });
        },

        getRealUsers: function () {
            return this.models.filter(function (user) {
                return !user.attributes.fake;
            });
        },

        getRealUsersButCurrent: function () {
            return this.models.filter(function (user) {
                return !user.attributes.fake && user.id != ServerListener.socketID;
            });
        }
    });

    return Users;
});
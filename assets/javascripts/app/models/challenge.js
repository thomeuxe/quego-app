define(function (require, exports, module) {

    var Backbone = require('backbone');
    var ServerListener = require('ServerListener');
    var event_bus = require('event_bus');
    var CurrentUser = require('CurrentUser');

    var Challenge = Backbone.Model.extend({

        initialize: function() {
            ServerListener.on('challenge:activated', this.delayedFetch.bind(this));

            this.url = ServerListener.serverURL + '/challenges/user/' + ServerListener.socketID + '/active';
            console.log(this.url);

            this.socketInited = false;

            this.delayedFetch();

        },

        delayedFetch: function() {
            TweenMax.delayedCall(1.2, function() {
                this.socketInited = false;
                this.fetch({
                    reset: true,
                    success: this.initSockets.bind(this),
                    error: function() {
                        this.clear();
                    }.bind(this)
                });
            }.bind(this));
        },

        initSockets: function() {
            console.log("InitSockets, listening to :");

            if(this.socketInited) {
                this.socketInit
                return;
            }

            this.socketInited = true;

            CurrentUser.model.activeChallenge = this.id;
            this.set('new', true);

            /**********
             * VALIDATION
             */

            ServerListener.once('challenge:validated:' + this.id, function(data){
                console.log("SOCKET RECEIVED");
                this.set('done', true);
                event_bus.trigger('challenge:done');
                this.delayedFetch();
            }.bind(this));

            /**********
             * MEETING
             */

            console.log('meeting:ok:' + this.id);

            ServerListener.on('meeting:ok:' + this.id, function(data){
                console.log('Meeting received !');
                this.set('meeting', data);
                event_bus.trigger('meeting:get', data);
            }.bind(this));

            console.log("END - InitSockets");

            /*********
             * CONNEXION ISSUE
             */

            this.listenTo(event_bus, 'connection:reconnect', this.delayedFetch.bind(this));
        },

        getTimeout: function() {
            if(!this.isTimeoutActive()) {
                return false;
            }

            var m = Math.floor(Math.floor((this.get('meeting').timeout - new Date().getTime())/1000)/60);
            var s = (Math.floor(Math.floor((this.get('meeting').timeout - new Date().getTime())/1000)) - m*60);

            return m + ":" + ((s < 10) ? "0" + s : s);
        },

        isTimeoutActive: function() {
            if(null != this.get('meeting')) {
                if(this.get('meeting').timeout - new Date().getTime() < 0) {
                    //this.set('meeting', null);
                    return false;
                } else {
                    return true;
                }
            }

            return false;
        }

    });

    return Challenge;
});
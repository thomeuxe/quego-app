define(function (require, exports, module) {

    var Backbone = require('backbone');

    var Event = Backbone.Model.extend({

        _id: null,
        name: null,
        location: null,
        adress: null,
        date: null,
        state: 1,
        participant_count: null

    });

    return Event;
});
define(function (require, exports, module) {

    var Backbone = require('backbone');
    var ServerListener = require('ServerListener');

    var User = Backbone.Model.extend({

        id: null,
        name: null,
        totem: null,
        anecdote: [],

        urlRoot: ServerListener.serverURL + '/users',

        initialize: function() {
            ServerListener.on('challenge:validatedBy:' + this.id, function(user) {
                this.set('validated', user.validated);
                this.trigger('change');
            }.bind(this));

            ServerListener.on('challenge:left:' + this.id, function(user) {
                this.set('available', user.available);
                this.trigger('change');
            }.bind(this));
        }

    });

    return User;
});
define(function (require, exports, module) {

    var Backbone = require('backbone');

    var event_bus = _({}).extend(Backbone.Events);

    return event_bus.__wrapped__;
});
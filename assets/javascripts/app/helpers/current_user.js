define(function (require, exports, module) {

    var Backbone = require('backbone');

    var CurrentUser = _({

        id: null,
        name: null,
        totem: null,
        user: null,
        favorites: JSON.parse(window.localStorage.getItem("dev:favorites")) || [],
        currentEvent: null,

        init: function() {
            this.name = window.localStorage.getItem("dev:name");
            this.avatar = "img/user-" + window.localStorage.getItem("dev:avatar") + ".jpg";
            this.totem = window.localStorage.getItem("dev:totem");
            this.anecdotes = JSON.parse(window.localStorage.getItem("dev:anecdotes"));
        },

        getId: function() {
            try {
                console.log("GETID--------------- " + this.model.id);
                return this.model.id;
            } catch(e) {
                console.log("GETID--------------- null");
                return null;
            }
        },

        addFavorite: function(ownerAttributes) {
            if(!_.find(this.favorites, function(fav) { return fav.id == ownerAttributes.id; }.bind(this))) {
                this.favorites.push(ownerAttributes);
                console.log(this.favorites);
                window.localStorage.setItem("dev:favorites", JSON.stringify(this.favorites));
                return true;
            }
            return false;
        }

    }).extend(Backbone.Events);

    return CurrentUser.__wrapped__;
});
define(function (require, exports, module) {

    var serverURL = "http://195.154.104.96:3000";
    //var serverURL = "http://localhost:3000";

    var Backbone = require('backbone');

    var CurrentUser = require('CurrentUser');
    var event_bus = require('event_bus');

    var io = require('socket.io');

    var ServerListener = {
        serverURL: serverURL,

        on: function (event, callback) {
            this.socket.on(event, callback);
        },

        once: function(event, callback) {
            this.socket.once(event, callback);
        },

        emit: function (event, data) {
            this.socket.emit(event, data);
        },

        init: function() {
            console.log(CurrentUser.name);

            this.socket = io.connect(serverURL, {query:"id=" + window.localStorage.getItem("dev:id") + "&name=" + CurrentUser.name + "&totem=" + CurrentUser.totem + "&avatar=" + CurrentUser.avatar + "&anecdotes=" + JSON.stringify(CurrentUser.anecdotes) });

            this.on('clients:self', function(user) {
                this.socketID = user.id;

                window.localStorage.setItem("dev:id", user.id);

                this.socket.io.opts.query = "id=" + user.id + "&name=" + CurrentUser.name + "&totem=" + CurrentUser.totem;

                CurrentUser.model = user;

                event_bus.trigger('socket:connect');


                if(window.localStorage.getItem("dev:name")) {
                    this.setDevSettings(window.localStorage.getItem("dev:name"), window.localStorage.getItem("dev:totem"));
                }

                console.log("your socket ID : " + ServerListener.socketID);
            }.bind(this));

            this.on('disconnect', function() {
                console.log('Connexion perdue...');
                event_bus.trigger('connection:disconnect');
            });

            this.on('reconnect', function() {
                console.log('Connexion retrouvée !');
                event_bus.trigger('connection:reconnect');
            });

            /**********
             * GUESS TOTEM
             */

            this.on('guessTotem:notify', function(data) {
                console.log("guessTotem:notify");
                event_bus.trigger('popup:guessTotem:guessed', data);
            });

            this.on('guessTotem:success', function(data) {
                event_bus.trigger('popup:guessTotem:success', data);
                console.log("You guessed right. meet at " + data.location + " in " + data.timeout + " minutes");
            });

            this.on('guessTotem:error', function() {
                event_bus.trigger('popup:guessTotem:error');
                console.log("You guessed WRONG !");
            });
        },

        setDevSettings: function(name, totem, avatar) {
            console.log(name, totem);
            this.emit('dev:settings:set', {
                name: name,
                totem: totem,
                avatar: avatar
            });
        },

        socketID: null
    };


    return ServerListener;
});
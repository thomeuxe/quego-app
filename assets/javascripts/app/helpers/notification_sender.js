define(function (require, exports, module) {

    var Backbone = require('backbone');

    var event_bus = require('event_bus');

    var NotificationSender = _.extend({

        lastId: 0,

        send: function (content, callback) {
            callback = callback || function() {};
            
            if(typeof cordova !== 'undefined') {
                cordova.plugins.notification.local.schedule({
                    id: this.lastId,
                    text: content,
                    icon: "file://img/icon.png",
                    smallIcon: "file://img/icon-small.png"
                });

                var i = this.lastId;

                cordova.plugins.notification.local.on("click", function(notification) {
                    if(notification.id == i)
                        callback();
                });

                return this.lastId;

                this.lastId++;
            } else {
                console.log(content);
            }
        },

        update: function(id, content) {
            if(typeof cordova !== 'undefined') {
                cordova.plugins.notification.local.update({
                    id: id,
                    text: content,
                    data: {updated: true}
                });
            } else {
                console.log("UPDATE PUSH : " + content);
            }
        },

        init: function() {
            /*********
             * CHALLENGE DONE
             */

            this.listenTo(event_bus, 'challenge:done', function () {
                this.send("Défi réussi !", function() {
                    event_bus.trigger('route', 'challenge');
                }.bind(this));
            }.bind(this));

            /*********
             * GUESS TOTEM
             */

            this.listenTo(event_bus, 'popup:guessTotem:guessed', function (data) {
                var pushId = this.send("On a découvert votre totem ! Rendez-vous " + data.location + " dans " + data.timeout + " minute" + (data.timeout > 1 ? "s": ""));

                function meetingTimeoutPush() {
                    window.setTimeout(function() {
                        data.timeout--;

                        if(data.timeout > 1) {
                            this.update(pushId, "On a découvert votre totem ! Rendez-vous " + data.location + " dans " + data.timeout + " minute" + (data.timeout > 1 ? "s": ""));
                            meetingTimeoutPush();
                        } else {
                            this.update(pushId, "On a découvert votre totem ! Rendez-vous maintenant " + data.location);
                        }
                    }.bind(this), 6000);
                }
            }.bind(this));

            /**********
             * MEETING
             */

            this.listenTo(event_bus, 'meeting:get', function(data) {
                var timeout = Math.floor(Math.floor((data.timeout - new Date().getTime())/1000)/60);
                var pushId = this.send("Rendez-vous " + data.location + " dans " + timeout + " minute" + (timeout > 1 ? "s": ""));

                function meetingTimeoutPush() {
                    window.setTimeout(function() {
                        var timeout = Math.floor(Math.floor((data.timeout - new Date().getTime())/1000)/60);

                        if(timeout > 1) {
                            this.update(pushId, "Rendez-vous " + data.location + " dans " + timeout + " minute" + (timeout > 1 ? "s": ""));
                            meetingTimeoutPush();
                        } else {
                            this.update(pushId, "Rendez-vous maintenant " + data.location);
                        }
                    }.bind(this), 6000);
                }

                meetingTimeoutPush.bind(this);
            }.bind(this));
        },

        sendNewChallengePush: function() {
            this.send("Vous avez reçu un nouveau défi !", function() {
                event_bus.trigger('route', 'challenge');
            }.bind(this));
        }
    }, Backbone.Events);


    return NotificationSender;
});
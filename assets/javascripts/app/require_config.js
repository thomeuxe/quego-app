require.config({
    deps: ['initialize_app'],
    paths: {
        'text': '../../../components/requirejs-plugins/lib/text',
        'async': '../../../components/requirejs-plugins/src/async',
        'font': '../../../components/requirejs-plugins/src/font',
        'goog': '../../../components/requirejs-plugins/src/goog',
        'image': '../../../components/requirejs-plugins/src/image',
        'json': '../../../components/requirejs-plugins/src/json',
        'noext': '../../../components/requirejs-plugins/src/noext',
        'mdown': '../../../components/requirejs-plugins/src/mdown',
        'propertyParser': '../../../components/requirejs-plugins/src/propertyParser',
        'markdownConverter': 'lib/Markdown.Converter',
        'backbone': '../../../components/backbone/backbone',
        'hammerjs': '../../../components/hammerjs/hammer',
        'PreventGhostClick': '../../../components/PreventGhostClick/index',
        'jquery-hammerjs': '../../../components/jquery-hammerjs/jquery.hammer-full',
        'backbone-hammer': '../../../components/backbone.hammer/backbone.hammer',
        'TweenMax': '../../../components_custom/gsap/src/uncompressed/TweenMax',
        'TweenLite': '../../../components_custom/gsap/src/uncompressed/TweenLite',
        'TimelineMax': '../../../components_custom/gsap/src/uncompressed/TimelineMax',
        'draggable': '../../../components_custom/gsap/src/uncompressed/utils/Draggable',
        'SplitText': '../../../components_custom/gsap/src/uncompressed/utils/SplitText',
        'throwprops': '../../../components_custom/gsap/src/uncompressed/plugins/ThrowPropsPlugin',
        'ScrollToPlugin': '../../../components_custom/gsap/src/uncompressed/plugins/ScrollToPlugin',
        'ScrollMagic': '../../../components/scrollmagic/scrollmagic/uncompressed/ScrollMagic',
        'ScrollMagicHelpers': '../../../components/scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators',
        'ScrollMagicGSAP': '../../../components/scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap',
        'underscore': '../../../components/lodash/dist/lodash',
        'Q': '../../../components/q/q',
        'jquery': '../../../components/jquery/dist/jquery',
        'moment': '../../../components/moment/moment',
        'event_bus': 'helpers/event_bus',
        'ServerListener': 'helpers/server_listener',
        'NotificationSender': 'helpers/notification_sender',
        'CurrentUser': 'helpers/current_user',
        'socket.io': '../../../components/socket.io-client/socket.io'
    },
    shim: {
        backbone: {
            deps: ['jquery', 'underscore'],
            exports: 'Backbone'
        },
        TweenLite: {
            exports: 'TweenLite'
        },
        draggable: {
            deps: ['TweenLite'],
            exports: 'Draggable'
        },
        SplitText: {
            deps: ['TweenLite'],
            exports: 'SplitText'
        },
        throwprops: {
            deps: ['TweenLite'],
            exports: 'ThrowProps'
        },
        ScrollToPlugin: {
            deps: ['TweenLite'],
            exports: 'ScrollToPlugin'
        },
        ScrollMagic: {
            exports: 'ScrollMagic'
        },
        Q: {
            exports: "Q"
        }
    }
});
define(function (require, exports, module) {

    var json = require('json');

    var Backbone = require('backbone');

    var event_bus = require('event_bus');
    var CurrentUser = require('CurrentUser');

    var MenuView = require('views/menu_view');
    var LoginView = require('views/login_view');
    
    var EventsView = require('views/events_view');
    var Events = require('collections/events');
    var _eventsData = require('json!data/events.json');

    var SettingsView = require('views/settings_view');
    var ChallengeView = require('views/challenge_view');
    var HistoryView = require('views/history_view');
    var AddPictureView = require('views/add_picture_view');
    var ScanView = require('views/scan_view');
    var AddMemoryView = require('views/add_memory_view');
    var PopupView = require('views/popup_view');

    var DevView = require('views/dev_view');

    var WidgetRouter = Backbone.Router.extend({

        routes: {
            "": "index", // This is a default route.
            "events": "events",
            "settings": "settings",
            "challenge": "challenge",
            "challenge/totem": "challengeTotem",
            "challenge/guessTotem": "challengeGuessTotem",
            "history(/:id)": "history",
            "history/:id/unlock": "historyUnlock",
            "addPicture": "addPicture",
            "scan": "scan",
            "addMemory/:id": "addMemory",
            "start": "addPicture",
            "popup/:id(/:opt)": "popup",
            "dev": "dev"
        },

        initialize: function (options) {
            this.app = options.app;
            this.listenTo(this, 'route', this.onRouteChange);

            this.listenTo(event_bus, 'addPicture', this.goToAddPicture.bind(this));

            this.listenTo(event_bus, 'popup:validatePicture', function () {
                this.goToValidatePicture();
            }.bind(this));

            this.listenTo(event_bus, 'route', function(route) {
                this.navigate(route, {trigger: true});
            }.bind(this));

            this.listenTo(event_bus, 'route:soft', function(route) {
                this.navigate(route);
            }.bind(this));
        },

        /*********
         * PAGE ROUTING
         */

        index: function () {
            console.log("Route: index");
            this.app.currentView = new LoginView();
            this.app.menuView.disable();
        },

        events: function () {
            console.log("Route: events");
            this.app.events = new Events(_eventsData);
            this.app.currentView = new EventsView({collection: this.app.events});
            this.app.menuView.enable();
        },

        settings: function() {
            console.log("Route: settings");
            this.app.currentView = new SettingsView();
            this.app.menuView.enable();
        },

        challenge: function() {
            console.log(_.isEmpty(app.challenge.attributes));
            app.challenge.fetch();

            this.app.currentView = new ChallengeView({
                model: this.app.challenge
            });
            this.app.currentView.active = "challenge";
            this.app.menuView.enable();
        },

        challengeTotem: function() {
            this.app.currentView = new ChallengeView({
                model: this.app.challenge
            });
            this.app.currentView.active = "totem";
            this.app.menuView.enable();
        },

        challengeGuessTotem: function() {
            this.challenge();

            TweenMax.delayedCall(1, function() {
                event_bus.trigger('popup:guessTotem');
            });
        },

        history: function(id) {
            id = id || CurrentUser.model.id;

            console.log(_.find(this.app.users.models, {
                'id': id
            }));

            this.app.currentView = new HistoryView({
                model: _.find(this.app.users.models, {
                    'id': id
                })
            });

            this.app.menuView.enable();
        },

        historyUnlock: function(id) {
            this.history(id);
            TweenMax.delayedCall(1, this.app.currentView.unlockAnecdote.bind(this.app.currentView));
        },

        addPicture: function() {
            this.app.currentView = new AddPictureView();
            this.app.menuView.enable();
        },

        goToAddPicture: function() {
            this.navigate("addPicture", {trigger: true});
        },

        goToValidatePicture: function() {
            this.navigate("popup/validatePicture");
        },

        scan: function() {
            this.app.currentView = new ScanView();
            this.app.menuView.enable();
        },

        addMemory: function(id) {
            this.app.currentView = new AddMemoryView({model: _.find(app.users.models, {id: id})});
            this.app.menuView.enable();
        },

        dev: function() {
            this.app.currentView = new DevView();
            this.app.menuView.enable();
        },

        popup: function(id, opt) {
            opt = opt || "";
            this.app.popupView.enabled = true;
            event_bus.trigger('popup:' + id, opt);
        },

        /**********
         * UTILS
         */

        renderCurrent: function() {
            console.log("Route: renderCurrent");

            this.app.renderContent();
            this.app.menuView.render();
        },

        execute: function(callback, args, name) {
            console.log(this.current(), this.app.popupView.enabled, this.app.currentView);
            if(this.current().route != "popup") { 
                this.app.previousView = this.app.currentView || {
                        exit: function (el, callback) {
                            console.log("Default exit");
                            callback();
                        }, remove: function () {
                        }
                    };
            }

            if (callback) callback.apply(this, args); //this must be called to pass to next route
        },

        onRouteChange: function() {
            console.log("Route: onRouteChange()");

            event_bus.trigger('route:change');

            this.app.previousView.exit = this.app.previousView.exit || function(el, callback) { callback(); };

            if (this.app.currentView != this.app.previousView || Backbone.history.fragment == "") {
                this.app.previousView.exit(this.app.$transitionEl[0], function () {
                    this.app.previousView.remove();
                    this.renderCurrent();
                    if (this.app.currentView.enter) {
                        this.app.currentView.enter(this.app.$transitionEl[0]);
                    }
                }.bind(this));
            }

            if(this.app.popupView.enabled) {
                this.app.popupView.enabled = false;
            }
        },

        current : function() {
            var Router = this,
                fragment = Backbone.history.fragment,
                routes = _.pairs(Router.routes),
                route = null, params = null, matched;

            matched = _.find(routes, function(handler) {
                route = _.isRegExp(handler[0]) ? handler[0] : Router._routeToRegExp(handler[0]);
                return route.test(fragment);
            });

            if(matched) {
                // NEW: Extracts the params using the internal
                // function _extractParameters
                params = Router._extractParameters(route, fragment);
                route = matched[1];
            }

            return {
                route : route,
                fragment : fragment,
                params : params
            };
        }

    });

    return WidgetRouter;
});

define(function (require, exports, module) {

    // Makes touch events in Backbone easy, see: https://github.com/wookiehangover/backbone.hammer
    require('hammerjs');
    require('jquery-hammerjs');
    require('backbone-hammer');
    require('TweenMax');
    require('draggable');
    require('SplitText');
    require('throwprops');
    require('ScrollToPlugin');
    require('ScrollMagic');
    require('ScrollMagicGSAP');
    require('PreventGhostClick');

    var _ = require('underscore');
    var $ = require('jquery');
    var Q = require('Q');
    var Backbone = require('backbone');
    var MainRouter = require('routers/main_router');
    var Events = require('collections/events');
    var Users = require('collections/users');
    var Challenge = require('models/challenge');

    var event_bus = require('event_bus');
    var ServerListener = require('ServerListener');
    var NotificationSender = require('NotificationSender');
    var CurrentUser = require('CurrentUser');

    var MenuView = require('views/menu_view');
    var LoginView = require('views/login_view');
    var EventsView = require('views/events_view');
    var SettingsView = require('views/settings_view');
    var ChallengeView = require('views/challenge_view');
    var HistoryView = require('views/history_view');
    var AddPictureView = require('views/add_picture_view');
    var ScanView = require('views/scan_view');
    var PopupView = require('views/popup_view');

    // We'll use this file to boot up our application. It's extending Backbone.View, but
    // isn't really used as a view at all. You'll want to replace all Backbone code in
    // this project with your own, it only exists to show you how requiring various
    // components in the application work together and it not intended to be an example
    // of a well structured or well built application. A sensible application architecture
    // is up to you, as it's not something Backbone really prescribes.
    var WidgetApp = Backbone.View.extend({

        initialize: function () {
            CurrentUser.init();

            NotificationSender.init();

            this.listenToOnce(event_bus, 'socket:connect', this.initApp.bind(this));

            ServerListener.init();
        },

        initApp: function() {
            this.$el = $('#app');
            this.$content = $('#page');

            this.$transitionEl = $('#pageTransition');

            this.currentView = null;

            /*******
             * CREATE OUR ROUTES
             */

            this.router = new MainRouter({
                app: this
            });

            /*******
             * CREATE OUR COLLECTIONS AND MODELS
             */

            this.users = new Users();

            this.favorites = new Users();

            this.challenge = new Challenge();

            /*******
             * CREATE OUR PERSISTANT VIEWS
             */

            this.menuView = new MenuView({
                challenge: this.challenge
            });

            this.popupView = new PopupView({
                app: this
            });

            /*******
             * FETCH REQUIRED DATA AND LAUNCH APP
             */

            Q.all([this.users.fetch({reset: true})])
                .then(function() {
                    TweenMax.delayedCall(2.5, function() {
                        this.$el.removeClass('loading');
                        TweenMax.to(this.$el.find('#loaderWrapper'), 0.8, {opacity: 0, force3D: true, onComplete: function() {
                            Backbone.history.start();
                        }});
                    }.bind(this));
                }.bind(this));

        },

        renderContent: function() {
            if(this.currentView) {
                this.$content.html(this.currentView.render().el);
            }
        }

    });

    return WidgetApp;
});
define(function (require, exports, module) {

    var Backbone = require('backbone');

    var FavoriteView = Backbone.View.extend({

        tagName: 'a',
        className: 'favorites__list__item',
        template: require('text!views/templates/favorite.html'),

        render: function () {
            console.log(this.model);
            this.$el.html(_.template(this.template)(this.model));

            this.$el.attr('href', '#history/' + this.model.id)

            _.defer(function () {
                this.tweenAppearance();
            }.bind(this));

            return this;
        },

        tweenAppearance: function () {
            TweenMax.fromTo(this.el, 0.5, {opacity: 0.5, scale: 0}, {
                opacity: 1,
                scale: 1,
                force3D: true,
                ease: Back.easeOut,
                delay: 0.2
            });
        }

    });

    return FavoriteView;
});
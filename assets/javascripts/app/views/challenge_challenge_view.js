define(function (require, exports, module) {

    var Backbone = require('backbone');

    var event_bus = require('event_bus');
    var ServerListener = require('ServerListener');
    var CurrentUser = require('CurrentUser');

    var ChallengeChallengerView = require('views/challenge_challenger_view');

    var ChallengeChallengeView = Backbone.View.extend({

        tagName: 'div',
        className: 'page page-challenge',
        template: require('text!views/templates/challenge.html'),
        templateEmpty: require('text!views/templates/challenge_empty.html'),

        hammerEvents: {
            'tap #validateChallenge': 'validateChallenge'
        },

        initialize: function (options) {
            this.users = options.users;

            this.challengerViews = [];

            this.listenTo(this.users, 'reset', this.createChallengersView.bind(this));
            //this.listenTo(this.model, 'change:done', this.challengeValidated.bind(this));
            this.listenToOnce(event_bus, 'challenge:done', this.challengeValidated.bind(this));
            this.listenTo(this.model, 'change:id', this.render.bind(this));

            this.i = 0;
        },

        render: function () {
            console.log(this.users);
            this.i++;
            console.log(this.i);
            if(this.model.get('id')) {
                console.log("PASSED HERE");
                this.$el.html(_.template(this.template)({challenge: this.model.attributes, currentUser: CurrentUser.model}));
                this.createChallengersView();
                _.defer(function () {
                    console.log("INIT LISTENERS");
                    this.$el.find('#validateChallenge').one('click', this.validateChallenge.bind(this));
                }.bind(this));
            } else {
                this.$el.html(this.templateEmpty);
            }


            return this;
        },

        createChallengersView: function() {
            console.log("CREATE CHALLENGE VIEW");

            this.challengerViews.forEach(function(view) {
                view.remove();
            });

            this.challengerViews = [];

            this.$el.find('#challengerWrapper').empty();

            this.users.models.forEach(function (user) {
                if(user.get('id') != ServerListener.socketID) {
                    this.challengerViews.push(new ChallengeChallengerView({model: user}));
                }
            }.bind(this));

            this.challengerViews.forEach(function(view) {
                this.$el.find('#challengerWrapper').append(view.render().el);
            }.bind(this));
        },

        validateChallenge: function () {
            console.log("validate");
            this.$el.find('#validateChallenge').addClass('validated', CurrentUser.model.validated);

            ServerListener.emit('challenge:validate', {
                userID: ServerListener.socketID,
                challengeID: this.model.id
            });
        },

        challengeValidated: function() {
            console.log(this.model.get('owner').id);
            event_bus.trigger('route', '#popup/challengeSuccess/' + this.model.get('owner').id);
        }

    });

    return ChallengeChallengeView;
});
define(function (require, exports, module) {

    var event_bus = require('event_bus');

    var CameraInterface = {

        tagName: "div",
        className: "innerPage",
        template: require('text!views/templates/selfie.html'),

        hammerEvents: {
            "tap #takePicture": "takePicture",
            "tap #switchCamera": "switchCameraSource"
        },

        render: function () {
            this.$el.html(this.template);

            _.defer(function() {
                this.initCamera();
            }.bind(this));

            return this;
        },

        /************
         * CAMERA INIT
         */

        initCamera: function () {
            var _self = this;

            this.initCanvas();

            this.video = $('<video></video>')[0];

            navigator.getUserMedia = ( navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia);

            this.currentCamera = -1;

            this.selectCameraSource();
        },

        /************
         * CANVAS INIT
         */

        initCanvas: function () {

            this.canvas = this.$el.find('#cameraCanvas')[0];
            this.canvas.width = screen.width;
            this.canvas.height = screen.width;

            this.ctx = this.canvas.getContext('2d');
        },

        /************
         * GETUSERMEDIA SOURCE SELECTION
         */

        selectCameraSource: function (currentCamera) {
            if(currentCamera === undefined) {
                currentCamera = -1;
            }

            MediaStreamTrack.getSources(function (sourceInfos) {
                var audioSource = null;
                var videoSource = null;

                console.log(sourceInfos);

                for (var i = 0; i < sourceInfos.length; i++) {
                    var sourceInfo = sourceInfos[i];
                    if (sourceInfo.kind === 'audio') {
                        console.log(sourceInfo.id, sourceInfo.label || 'microphone');

                        audioSource = sourceInfo.id;
                    } else if (sourceInfo.kind === 'video') {
                        console.log(sourceInfo, sourceInfo.label || 'camera');

                        if(currentCamera != i) {
                            this.currentCamera = i;
                            $(this.canvas).attr('data-facing', sourceInfo.facing);
                            videoSource = sourceInfo.id;
                        }
                    } else {
                        console.log('Some other kind of source: ', sourceInfo);
                    }
                }

                this.startCamera(audioSource, videoSource);
            }.bind(this));
        },

        switchCameraSource: function() {
            this.stopCamera();

            this.selectCameraSource(this.currentCamera);
        },

        /************
         * START GETUSERMEDIA
         */

        startCamera: function (audioSource, videoSource) {

            var constraints = {
                audio: false,
                video: {
                    optional: [{sourceId: videoSource}]
                }
            };

            navigator.getUserMedia(constraints, this.startCameraSuccess.bind(this), function () {
                console.error("getUserMedia() not available");
            });
        },

        startCameraSuccess: function (localMediaStream) {
            this.stream = localMediaStream;
            this.rafID = 0;
            this.video.src = window.URL.createObjectURL(this.stream);
            this.video.play();

            _.defer(function() {
                window.requestAnimationFrame(this.canvasRender.bind(this));
            }.bind(this));

            console.log(this.video.src);
        },

        /************
         * DRAW VIDEO IN CANVAS
         */

        canvasRender: function () {
            this.rafID = window.requestAnimationFrame(this.canvasRender.bind(this));
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.ctx.drawImage(this.video, - (this.video.videoWidth - this.canvas.width) / 2, - (this.video.videoHeight - this.canvas.height) / 2, this.video.videoWidth, this.video.videoHeight);
        },

        /************
         * PAGE EVENTS
         */

        takePicture: function() {
            event_bus.trigger('popup:validatePicture', {base64: this.canvas.toDataURL("image/png")});
        },

        /************
         * PAGE TRANSITION
         */

        stopCamera: function() {
            console.log("STOPCAMERA");
            window.cancelAnimationFrame(this.rafID);
            console.log(this.stream.getVideoTracks());
            this.stream.getVideoTracks()[0].stop();
        },

        exit: function(el, callback) {
            console.log("EXIT CAMERA INTERFACE");
            this.stopCamera.bind(this);
            window.cancelAnimationFrame(this.rafID);
            this.rafID = undefined;
            this.stream.getVideoTracks()[0].stop();
            callback();
        }

    };

    return CameraInterface;
});
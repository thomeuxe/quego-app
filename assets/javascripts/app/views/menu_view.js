define(function (require, exports, module) {

    require('moment');

    var Backbone = require('backbone');
    var event_bus = require('event_bus');

    var CurrentUser = require('CurrentUser');
    var ServerListener = require('ServerListener');

    var LoginView = Backbone.View.extend({
        el: "#menu",
        enabled: false,

        hammerEvents: {
            "tap #lateralMenuToggle": "openMenu",
            "tap #headerNav a": "closeMenu",
            "tap #headerNavOverlay": "closeMenu",
            "tap #headerLogo": "reloadPage",
            "tap #leave": "leave"
        },

        template: require('text!views/templates/menu.html'),

        initialize: function (opt) {
            this.challenge = opt.challenge;

            this.listenTo(this.challenge, 'change:meeting', this.initTimer.bind(this));
        },

        render: function () {
            console.log(this.rendered, this.enabled);
            if (!this.rendered && this.enabled) {
                this.$el.html(_.template(this.template)({
                    user: CurrentUser.model
                }));
                this.rendered = true;
                this.initListener();
            } else if (!this.enabled) {
                this.$el.empty();
                this.rendered = false;
            }
        },

        enable: function () {
            this.enabled = true;
        },

        disable: function () {
            this.enabled = false;
        },

        initListener: function () {
            this.$headerNav = this.$el.find('#headerNav');
            this.headerNavWidth = this.$headerNav.width();
            this.$headerNavOverlay = this.$el.find('#headerNavOverlay');

            var _self = this;

            //Init menu timelineMax

            var tl = new TimelineMax({paused: true});

            tl.fromTo(this.$headerNavOverlay[0], 1, {autoAlpha: 0}, {autoAlpha: 1, force3D: true});

            //Init draggable

            if (this.$headerNav.length) {
                this.draggable = Draggable.create(this.$headerNav[0], {
                    type: 'x',
                    edgeResistance: 0.98,
                    throwProps: true,
                    trigger: '#headerNavTrigger, #headerNavOverlay, #headerNav',
                    maxDuration: 0.4,
                    snap: {
                        x: function (endValue) {
                            return Math.round(endValue / _self.headerNavWidth) * _self.headerNavWidth;
                        }
                    },
                    updateTweens: function (x) {
                        var alpha = 1 - Math.abs(x / _self.headerNavWidth);
                        tl.seek(Math.min(Math.max(1 - Math.abs(x / _self.headerNavWidth), 0), 1));
                    },
                    onDrag: function () {
                        this.vars.updateTweens(this.x);
                    },
                    onThrowUpdate: function () {
                        this.vars.updateTweens(this.x);
                    },
                    bounds: {left: -_self.$headerNav.width(), top: 0, height: 1000, width: _self.$headerNav.width() * 2}
                });
            }

            //Init timer

            if (null != this.challenge.get('meeting')) {
                this.initTimer();
            }
        },

        /*********
         * MEETING TIMER
         */

        initTimer: function () {
            if (null != this.challenge.get('meeting')) {
                this.renderTimer();

                TweenMax.to(this.$el.find('#meetingTimeout'), 0.5, {
                    y: "-50%",
                    ease: Back.easeOut
                });
            }
        },

        renderTimer: function () {
            if (this.challenge.getTimeout()) {
                this.raf = window.requestAnimationFrame(this.renderTimer.bind(this));
                this.$el.find('#meetingTimeout').html(this.challenge.getTimeout());
            } else {
                TweenMax.to(this.$el.find('#meetingTimeout'), 0.5, {
                    y: -50, ease: Back.easeIn, onComplete: function () {
                        this.$el.find('#meetingTimeout').empty();
                    }.bind(this)
                });
            }
        },

        /************
         * PAGE EVENTS
         */

        reloadPage: function() {
            window.location.reload(false);
        },

        leave: function() {
            event_bus.trigger('popup:leave');
        },

        /************
         * PAGE TRANSITIONS
         */

        openMenu: function () {
            TweenMax.to(this.$headerNav, 0.3, {x: 0, ease: Back.easeOut});
            TweenMax.to(this.$headerNavOverlay, 0.3, {autoAlpha: 1});
        },

        closeMenu: function () {
            TweenMax.to(this.$headerNav, 0.3, {x: -this.headerNavWidth});
            TweenMax.to(this.$headerNavOverlay, 0.3, {autoAlpha: 0});
        }

    });

    return LoginView;
});
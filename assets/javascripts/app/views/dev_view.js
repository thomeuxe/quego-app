define(function (require, exports, module) {

    var Backbone = require('backbone');

    var ServerListener = require('ServerListener');
    var CurrentUser = require('CurrentUser');

    /************
     * DEVELOPMENT VIEW TO SET USER SPECIFIC DATA
     */
    var DevView = Backbone.View.extend({
        tagName: "div",
        className: "innerPage page-dev",
        template: require('text!views/templates/dev.html'),

        initialize: function () {

        },

        render: function () {
            this.$el.html(_.template(this.template)());

            _.defer(function() {
                this.initListeners();
            }.bind(this));

            return this;
        },

        initListeners: function() {
            this.$el.find('#devForm').on('submit', this.submitForm.bind(this));

            this.$el.find('#devUsername').val(window.localStorage.getItem("dev:name"));
            this.$el.find('#devTotem').val(window.localStorage.getItem("dev:totem"));
            this.$el.find('#devAvatar').val(window.localStorage.getItem("dev:avatar"));
        },

        submitForm: function(e) {
            e.preventDefault();

            var name = this.$el.find('#devUsername').val();
            var totem = this.$el.find('#devTotem').val();
            var avatar = this.$el.find('#devAvatar').val();

            window.localStorage.setItem("dev:name", name);
            window.localStorage.setItem("dev:totem", totem);
            window.localStorage.setItem("dev:avatar", avatar);

            ServerListener.setDevSettings(name, totem, avatar);
        },

        exit: function (el, callback) {
            callback();
        }

    });

    return DevView;
});
define(function (require, exports, module) {

    var Backbone = require('backbone');

    var CurrentUser = require('CurrentUser');

    var EventsView = Backbone.View.extend({
        id: "js-events-view",
        tagName: "div",
        className: "inner-content",

        hammerEvents: {
            "tap .event-card[data-state=0] .event-card__button": "setCurrentEvent"
        },

        template: require('text!views/templates/events.html'),

        render: function () {
            this.$el.html(_.template(this.template)({
                events: _.pluck(this.collection.models, 'attributes'),
                user: CurrentUser
            }));

            this.initListeners();

            return this;
        },

        initListeners: function () {
            this.$eventList = this.$el.find('#eventList');
            this.$eventListBullets = this.$el.find('#eventListBullets').find('.event-list__bullet');
            var $events = this.$eventList.children('*');
            var eventNb = $events.length;
            var gridWidth = window.innerWidth;
            var currentSlide = 0;

            $events.first().addClass('event-card--active');

            var $currentSlide = $events.first();
            var currentSlideEls = {
                $header: $currentSlide.find('.event-card__header')
            };

            /**************
             * Init background animation
             */

            TweenMax.set(this.$el.find('.page__background__inner'), {z: 0, force3D: true});

            var tl = new TimelineMax();
            tl.pause();

            this.$el.find('.page__background__item').each(function (i, el) {

                if (prevEl) {
                    tl.staggerTo($(prevEl).find('.page__background__item__line'), 1, {
                        x: '-=100%',
                        yoyo: true,
                        ease: Sine.easeInOut,
                        force3D: true
                    }, 1 / 15, (i - 1) * 2);
                    tl.staggerTo($(el).find('.page__background__item__line'), 1, {
                        x: '-=100%',
                        yoyo: true,
                        ease: Sine.easeInOut,
                        force3D: true
                    }, 1 / 15, (i - 1) * 2);

                    el.hasTween = true;

                    tl.add(TweenMax.set($(el).nextAll().find('.page__background__item__line'), {x: '-=100%'}), (i - 1) * 2);
                    tl.set(this.$eventListBullets[i - 1], {className: "-=active"}, Math.max((i - 1) * 2 + 1, 0));
                }

                tl.set(this.$eventListBullets[i], {className: "+=active"}, Math.max((i - 1) * 2 + 1, 0));

                prevEl = el;
            }.bind(this));

            tl.seek(0.01);

            var prevEl = null;

            /**************
             * Init events slider
             */

            var eventListDraggable = Draggable.create(this.$el.find('#eventList')[0], {
                type: 'x',
                edgeResistance: 0.9,
                dragResistance: 0.1,
                throwProps: true,
                maxDuration: 0.4,
                bounds: {left: -(eventNb - 1) * window.innerWidth, width: eventNb * window.innerWidth},
                trigger: this.$el.find('.event-card__header, .event-card__remember, .event-card__meta'),
                snap: {
                    x: function (endValue) {
                        return Math.round(endValue / gridWidth) * gridWidth;
                    }
                },
                onDrag: function (e) {
                    tl.seek(Math.max(-this.x / window.innerWidth * 2, 0));
                },
                onThrowUpdate: function () {
                    tl.seek(Math.max(-this.x / window.innerWidth * 2, 0));
                },
                onDragEnd: function (e) {
                    var offsetX = this.x;

                    $events.css('background', '');
                    $events.removeClass('event-card--active');

                    currentSlide += (this.endX, this.endX ? this.endX < 0 ? 1 : -1 : 0);

                    if (currentSlide < 0)
                        currentSlide = 0;
                    if (currentSlide >= eventNb)
                        currentSlide = eventNb;

                    $currentSlide = $($events[currentSlide]);
                    $currentSlide.addClass('event-card--active');
                    currentSlideEls.$header = $currentSlide.find('.event-card__header');
                }
            });

            var i = $(this.$eventList.find('.event-card[data-state=0]').index());
            TweenMax.set(this.$eventList[0], {x: -i[0] * window.innerWidth});
            eventListDraggable[0].update();
            tl.seek(i[0] * 2);

            /**************
             * Init UI tween
             */

            var wiggleTl = new TimelineMax({delay: 4, repeat: -1, repeatDelay: 3});

            var $startButtons = this.$el.find('.event-card[data-state=0]').find('.event-card__button');

            wiggleTl.to($startButtons, 0.05, {rotationZ: 6, x: -3})
                .to($startButtons, 0.07, {rotationZ: -6, x: 3, yoyo: true, repeat: 4})
                .to($startButtons, 0.1, {rotationZ: 0, x: 0, ease: Bounce.easeOut})

            /**************
             * Init settings
             */

            this.initSettings();
        },

        initSettings: function () {

            var _self = this;

            this.$el.find('.profile__progress-bar__inner').each(function (i, el) {
                TweenMax.set(el, {width: $(el).data('percentage') + '%'});
            });

            this.$eventList.find('.event-card__footer').each(function (i, el) {

                $(el).on('click', _self.openFooter);
                $(el).closest('.event-card').find('.event-card__header').on('click', _self.closeFooter.bind(el));

                TweenMax.set([$(el).find('.event-card__footer__header')[0], $(el).closest('.event-card').find('.event-card__header')[0], $(el).closest('.event-card').find('.event-card__totem')[0], $(el).closest('.event-card').find('.event-card__meta')[0], $('#pageBackground')[0]], {
                    z: 0,
                    force3D: true
                });

                var tl = new TimelineMax({paused: true});

                tl.to($(el).find('.event-card__footer__header')[0], 1, {
                        autoAlpha: 0,
                        y: window.innerHeight / 3,
                        force3D: true
                    }, 0)
                    .to($(el).closest('.event-card').find('.event-card__header')[0], 1, {
                        y: -window.innerHeight / 2.3,
                        ease: Quad.easeInOut,
                        force3D: true
                    }, 0)
                    .to($(el).closest('.event-card').find('.event-card__totem')[0], 0.8, {
                        autoAlpha: 0,
                        y: window.innerHeight / 10,
                        scale: 0.8,
                        ease: Quad.easeInOut,
                        force3D: true
                    }, 0)
                    .to($(el).closest('.event-card').find('.event-card__meta')[0], 0.8, {
                        autoAlpha: 0,
                        //y: window.innerHeight / 20,
                        rotationX: -45,
                        force3D: true
                    }, 0)
                    .to(_self.$el.find('#pageBackground').find('.page__background__item')[_self.$el.find('.event-card').index($(el).closest('.event-card'))], 1, {
                        y: -window.innerHeight / 4,
                        force3D: true
                    }, 0);

                el.gsapDraggable = Draggable.create(el, {
                    type: 'y',
                    throwProps: true,
                    maxDuration: 0.5,
                    edgeResistance: 0.9,
                    dragResistance: -0.3,
                    zIndexBoost: false,
                    trigger: $(el).closest('.event-card').find('.event-card__header, .event-card__footer'),
                    snap: {
                        y: function (endValue) {
                            var height = $(this.target).find('.event-card__profile').outerHeight() + $(this.target).outerHeight();
                            return (endValue, endValue ? endValue < this.minY / 2 ? -1 : 1 : 0) * height;
                        }
                    },
                    onDragStart: function () {
                        var height = $(this.target).find('.event-card__profile').outerHeight() + $(this.target).outerHeight();
                        this.applyBounds({
                            left: 0,
                            top: window.innerHeight - height - $('#header').height(),
                            height: height
                        });
                    },
                    updateTweens: function (y) {
                        tl.seek(Math.max(y / window.innerHeight, 0));
                    },
                    onDrag: function () {
                        this.vars.updateTweens(-this.y);
                    },
                    onThrowUpdate: function () {
                        this.vars.updateTweens(-this.y);
                    }
                });
            });
        },

        openFooter: function () {
            var el = this;
            TweenMax.to(el, 0.6, {
                y: -$(el).find('.event-card__profile').outerHeight(), onUpdate: function () {
                    el.gsapDraggable[0].vars.updateTweens(-el._gsTransform.y);
                }
            });
            console.log(el.gsapDraggable[0]);
        },

        closeFooter: function () {
            var el = this;
            TweenMax.to(el, 0.6, {
                y: 0, onUpdate: function () {
                    el.gsapDraggable[0].vars.updateTweens(-el._gsTransform.y);
                }
            });
            console.log(el.gsapDraggable[0]);
        },

        setCurrentEvent: function (e) {
            CurrentUser.currentEvent = $(e.target).closest('.event-card').data('index');
        },

        /************
         * PAGE TRANSITIONS
         */

        enter: function (el) {
            var $activeEvent = this.$el.find('.event-card[data-state=0]').first();

            var $subtitleEffect = $activeEvent.find('.event-card__subtitle__effect');

            var headerText = new SplitText($activeEvent.find('.event-card__title'), {type: "chars"});

            TweenMax.set($activeEvent.find('.event-card__subtitle__text, .event-card__totem'), {
                opacity: 0,
                force3D: true
            });

            var tl = new TimelineMax({delay: 1.5});

            tl.fromTo($activeEvent.find('.event-card__totem'), 1, {y: 80, opacity: 0}, {
                    y: 0,
                    opacity: 1,
                    ease: Quad.easeOut,
                    force3D: true
                })
                .staggerFrom(headerText.chars, 0.23, {
                    y: 20,
                    scale: 0.8,
                    opacity: 0,
                    delay: 0.2,
                    force3D: true
                }, 0.05, "-=0.7")
                .set($subtitleEffect, {transformOrigin: "0 50%"})
                .fromTo($subtitleEffect, 0.2, {scaleX: 0}, {scaleX: 1, ease: Quad.easeIn, delay: 0.6, force3D: true})
                .set($activeEvent.find('.event-card__subtitle__text'), {opacity: 1})
                .set($subtitleEffect, {transformOrigin: "100% 50%"})
                .to($subtitleEffect, 0.2, {scaleX: 0, ease: Quad.easeOut, force3D: true});
        },

        exit: function (el, callback) {
            $(el).empty();
            var tEl = document.createElement("div");
            $(tEl).addClass('login-btn');
            $(el).append(tEl);

            TweenMax.fromTo(tEl, 0.5, {
                scale: 0, y: "50%", x:"-50%", force3D: true
            },
                {
                scale: 2, ease: Quad.easeIn, force3D: true, onComplete: function () {
                    callback();

                    _.defer(function () {
                        TweenMax.to(tEl, 0.7, {y: "-150%", scale: 0.6, force3D: true, ease: Quad.easeIn, delay: 0.1});
                    });
                }
            });
        }

    });

    return EventsView;
});
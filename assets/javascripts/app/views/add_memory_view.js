define(function (require, exports, module) {

    var Backbone = require('backbone');
    var _ = require('underscore');

    var event_bus = require('event_bus');

    var CameraInterface = require('views/interfaces/camera_interface');

    /***********
     * @extends CameraInterface
     */
    var AddMemoryView = Backbone.View.extend(_.extend(CameraInterface, {

        template: require('text!views/templates/add_memory.html'),

        hammerEvents: {
            "tap #takePicture": "takePicture",
            "tap #switchCamera": "switchCameraSource"
        },

        render: function () {
            this.$el.html(this.template);

            _.delay(function() {
                this.initCamera();
            }.bind(this));

            return this;
        },

        initCanvas: function () {
            this.canvas = document.getElementById('cameraCanvas');
            this.canvas.width = screen.width;
            this.canvas.height = screen.height;

            this.ctx = this.canvas.getContext('2d');
        },

        canvasRender: function () {
            this.rafID = window.requestAnimationFrame(this.canvasRender.bind(this));
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.ctx.drawImage(this.video, - (this.video.videoWidth - this.canvas.width) / 2, - (this.video.videoHeight - this.canvas.height) / 2, this.video.videoWidth, this.video.videoHeight);
        },

        takePicture: function() {
            event_bus.trigger('popup:validateMemory', {base64: this.canvas.toDataURL("image/png"), userID: this.model.id});
        }

    }));

    return AddMemoryView;
});
define(function (require, exports, module) {

    var PopupView;
    var Backbone = require('backbone');
    var Q = require('Q');

    var event_bus = require('event_bus');
    var ServerListener = require('ServerListener');
    var CurrentUser = require('CurrentUser');

    PopupView = Backbone.View.extend({
        el: "#popup",

        hammerEvents: {
            "tap #popupOverlay": "hidePopupNavigate",
            "tap #popupOverlaySoft": "hidePopup",
            "tap #closePopup": "hidePopupNavigate",
            "tap #closePopupSoft": "hidePopup",
            "tap #sendMeeting": "sendMeeting",
            "tap #sendGuessTotem": "sendGuessTotem",
            "tap #acceptGuessMeeting": "acceptGuessMeeting",
            "tap #declineGuessMeeting": "declineGuessMeeting",
            "tap #addPicture": "addPicture",
            "tap #addMemoryHistory": "addMemoryToHistory",
            "click #nextChallenge": "nextChallenge",
            "tap #sendAnecdote": "sendAnecdote",
            "tap #leaveButton": "leaveButton"
            //"click #nextChallenge": "stopPropagation"
        },

        initialize: function (options) {
            this.templates = {};
            this.waitForRender = false;

            this.enabled = false;

            this.app = options.app;

            console.log(event_bus);

            this.listenTo(event_bus, 'popup:challengeSuccess', function (ownerID) {
                this.render('challengeSuccess', {ownerID: ownerID});
            }.bind(this));

            this.listenTo(event_bus, 'popup:newTotem', function (opt) {
                this.newTotem(opt);
            }.bind(this));

            this.listenTo(event_bus, 'popup:validatePicture', function (opt) {
                this.validatePicture(opt);
            }.bind(this));

            this.listenTo(event_bus, 'popup:validateMemory', function (opt) {
                this.validateMemory(opt);
            }.bind(this));

            this.listenTo(event_bus, 'popup:meeting', function (opt) {
                this.meeting();
            }.bind(this));

            this.listenTo(event_bus, 'popup:guessTotem', function (opt) {
                this.guessTotem();
            }.bind(this));

            this.listenTo(event_bus, 'popup:guessTotem:guessed', function (opt) {
                this.guessedTotem(opt);
            }.bind(this));

            this.listenTo(event_bus, 'popup:guessTotem:success', function(opt) {
                this.guessTotemSuccess(opt);
            });

            this.listenTo(event_bus, 'popup:guessTotem:error', function(opt) {
                this.guessTotemError(opt);
            });

            this.listenTo(event_bus, 'popup:anecdote', function (opt) {
                this.render('anecdote');
            }.bind(this));

            this.listenTo(event_bus, 'popup:leave', function (opt) {
                this.render('leave');
            }.bind(this));

            this.listenTo(event_bus, 'route:change', function (opt) {
                if(!this.$el.is(':empty'))
                    this.hidePopup();
            }.bind(this));

            var _self = this;

            Q.all([$.get("js/app/views/templates/popups/no_internet.html"), $.get("js/app/views/templates/popups/challenge_success.html"),  $.get("js/app/views/templates/popups/new_totem.html"), $.get("js/app/views/templates/popups/validate_picture.html"), $.get("js/app/views/templates/popups/validate_memory.html"), $.get("js/app/views/templates/popups/meeting.html"), $.get("js/app/views/templates/popups/anecdote.html"), $.get("js/app/views/templates/popups/guess_totem.html"), $.get("js/app/views/templates/popups/guessed_totem.html"), $.get("js/app/views/templates/popups/guess_totem_success.html"), $.get("js/app/views/templates/popups/guess_totem_error.html"), $.get("js/app/views/templates/popups/leave.html")])
                .then(function (res) {
                    res.forEach(function (html) {
                        _self.registerTemplate(html);
                    });

                    if (_self.waitForRender && _self.templates[_self.waitForRender]) {
                        _self.render(_self.waitForRender);
                        this.waitForRender = false;
                    }
                });
        },

        render: function (templateID, opt) {

            opt = opt || {};

            if (this.templates[templateID]) {
                this.$el.html(_.template(this.templates[templateID], opt));
                TweenMax.delayedCall(0.2, this.showPopup.bind(this));
            } else {
                this.waitForRender = templateID;
            }
        },

        registerTemplate: function (html) {
            var id = $(html).first().attr('id');

            this.templates[id] = html;
        },

        showPopup: function () {
            this.enabled = true;
            console.log("showPopup");
            var $popup = this.$el.find('#popupInner');
            var $popupOverlay = this.$el.find('#popupOverlay, #popupOverlaySoft');

            TweenMax.killTweensOf($popup[0]);
            TweenMax.set([$popupOverlay, $popup[0]], {z: 0, autoAlpha: 1, force3D: true});
            TweenMax.fromTo($popupOverlay, 0.5, {scale: 0, force3D: true}, {scale: 1, force3D: true, delay: 0.1});
            TweenMax.fromTo($popup, 0.4, {scale: 0, autoAlpha: 0, force3D: true}, {scale: 1, autoAlpha: 1, force3D: true, ease: Back.easeInOut});
        },

        hidePopup: function () {
            var $popup = this.$el.find('#popupInner');
            var $popupOverlay = this.$el.find('#popupOverlay, #popupOverlaySoft');

            TweenMax.to($popup, 0.3, {scale: 0.7, ease: Back.easeIn, autoAlpha: 0, force3D: true});
            //TweenMax.to($popupOverlay[0], 0.4, {autoAlpha: 0, force3D: true});
            TweenMax.to($popupOverlay, 0.5, {scale: 0, force3D: true});
        },

        hidePopupNavigate: function () {
            this.hidePopup();

            window.history.back();
        },

        /************
         * POPUP SPECIFIC METHOD
         */

        sendMeeting: function() {
            ServerListener.emit('meeting', {
                challengeID: CurrentUser.model.activeChallenge,
                location: this.$el.find('.totem-slide.active').data('location'),
                timeout: this.$el.find('.time-slide.active').data('timeout')
            });

            this.hidePopup();
        },

        addPicture: function () {
            event_bus.trigger('addPicture');
            this.hidePopupNavigate();
        },

        nextChallenge: function (e) {
            e.stopPropagation();
            e.preventDefault();
            if(this.app.challenge.id && this.app.challenge.get('new')) {
                this.app.router.navigate('popup/newTotem');
                event_bus.trigger('popup:newTotem', {owner: this.app.challenge.attributes.owner});
            } else {
                this.hidePopup();
            }
        },

        validatePicture: function (opt) {
            if(opt && opt.base64) {
                this.render('validatePicture', {base64: opt.base64});
                this.$el.find('#pictureImg').attr('src', this.$el.find('#pictureImg').data('src'));
            } else {
                window.history.back();
            }
        },

        validateMemory: function (opt) {
            console.log(opt);
            if(opt && opt.base64) {
                this.memoryBase64 = opt.base64;
                this.memoryUserID = opt.userID;
                this.render('validateMemory', {base64: opt.base64});
                this.$el.find('#pictureImg').attr('src', this.$el.find('#pictureImg').data('src'));
            } else {
                window.history.back();
            }
        },

        addMemoryToHistory: function() {
            app.users.fetch({
                reset: true,
                success: function() {
                    app.router.navigate('history/' + this.memoryUserID + '/unlock', {trigger: true});
                }.bind(this)
            })
        },

        newTotem: function () {
            if(this.app.challenge.get('new')) {
                this.app.challenge.set('new', false);
                this.render('newTotem', {owner: this.app.challenge.attributes.owner});
                this.$el.find('#totemImage').attr('src', this.$el.find('#totemImage').data('src'));
            } else {
                window.history.back();
            }
        },

        meeting: function() {
            this.render('meeting');

            this.sliderPopupInit();
        },

        /*********
         * TOTEM GUESSING
         */

        guessTotem: function () {
            this.render('guessTotem', {users: this.app.users.models});

            _.defer(function() {
                this.sliderPopupInit();
            }.bind(this));
        },

        guessedTotem: function (meeting) {
            this.guessMeeting = meeting;
            this.render('guessedTotem', {users: this.app.users.models, meeting: meeting});
        },

        sendGuessTotem: function() {
            console.log({
                totemID: this.$el.find('#totemSliderWrapper').find('.totem-slide.active').data('id'),
                nameID: this.$el.find('#userAutocompleteId').val(),
                location: "À la table de billard",
                timeout: this.$el.find('#timeSliderWrapper').find('.time-slide.active').data('timeout')
            });

            ServerListener.emit('guessTotem', {
                totemID: this.$el.find('#totemSliderWrapper').find('.totem-slide.active').data('id'),
                nameID: this.$el.find('#userAutocompleteId').val(),
                location: "À la table de billard",
                timeout: this.$el.find('#timeSliderWrapper').find('.time-slide.active').data('timeout')
            });

            this.hidePopupNavigate();
        },

        acceptGuessMeeting: function(e) {
            ServerListener.emit('guessTotem:meeting:accept', this.guessMeeting);

            this.hidePopup();
        },

        declineGuessMeeting: function() {
            ServerListener.emit('guessTotem:meeting:decline', this.guessMeeting);

            this.hidePopup();
        },

        guessTotemSuccess: function (meeting) {
            this.render('guessTotemSuccess', {meeting: meeting});
        },

        guessTotemError: function (meeting) {
            this.render('guessTotemError', {meeting: meeting});
        },
        
        leaveButton: function() {
            ServerListener.emit('leave');
            event_bus.trigger('route', '/');
        },

        /***********
         * ANECDOTES
         */

        sendAnecdote: function (opt) {
            var newAnecdote = {
                category: this.$el.find('#anecdoteCategory').val(),
                content: this.$el.find('#anecdoteContent').val()
            };

            console.log(newAnecdote);

            var storedAnecdotes = JSON.parse(window.localStorage.getItem("dev:anecdotes")) || [];

            if(storedAnecdotes)

            console.log(storedAnecdotes);

            var anecdotes = storedAnecdotes;
            anecdotes.push(newAnecdote);

            console.log(anecdotes);

            window.localStorage.setItem('dev:anecdotes', JSON.stringify(anecdotes));

            ServerListener.emit('anecdote:new', newAnecdote);

            TweenMax.delayedCall(0.2, function() {
                event_bus.trigger('route', 'history');
            });
        },

        stopPropagation: function(e) {
            e.stopPropagation();
            e.preventDefault();
        },

        /***********
         * POPUP WITH TOUCH SLIDER
         */

        sliderPopupInit: function() {
            var _self = this;

            var $totemSlideEl = this.$el.find('.totem-slide');
            var totemSlideElCount = $totemSlideEl.length;

            var totemSnapWidth = $totemSlideEl.first().addClass('active').outerWidth(true);

            var tl = new TimelineMax();

            $totemSlideEl.each(function (i) {
                tl.to(this, 0.5, {scale: 1.7, backgroundColor: '#f61469', color: '#f61469', ease: Sine.easeInOut}, i - 0.5);
                tl.to(this, 0.5, {scale: 1, backgroundColor: '#ffffff', color: '#fff', ease: Sine.easeInOut}, i + 0.5);
            });

            tl.pause();

            tl.seek(0.5);
            tl.timeScale(5);

            var totemDraggable = Draggable.create(this.$el.find('#totemSlider')[0], {
                type: 'x',
                edgeResistance: 0.9,
                dragResistance: 0.1,
                throwProps: true,
                trigger: '#totemSliderWrapper',
                snap: {
                    x: function (endX) {
                        var activeElIndex = Math.max(Math.min(Math.round(endX / totemSnapWidth), 0), -totemSlideElCount + 1);
                        tl.seek(Math.abs(activeElIndex) + 0.5);
                        $totemSlideEl.removeClass('active');
                        $totemSlideEl.filter(':nth-child(' + Math.abs(activeElIndex - 1) + ')').addClass('active');
                        return activeElIndex * totemSnapWidth;
                    }
                },
                updateCurrentEl: function (x) {
                    tl.seek(-Math.max(Math.min(x / totemSnapWidth, 0), -totemSlideElCount + 1) + 0.5);
                },
                onDrag: function () {
                    this.vars.updateCurrentEl(this.x);
                },
                onThrowUpdate: function () {
                    this.vars.updateCurrentEl(this.x);
                }
            });

            $totemSlideEl.on('click', function () {
                tl.tweenTo($(this).index() + 0.5);
                TweenMax.to(_self.$el.find('#totemSlider')[0], 0.3, {
                    x: -$(this).index() * totemSnapWidth,
                    ease: Quad.easeInOut
                });
            });

            /****************
             * User autocomplete
             */

            var _self = this;

            $('#userAutocompleteList').on('click', '.user-form__autocomplete__item', function() {
                $('#userAutocompleteId').val($(this).data('id'));
                $('#userAutocompleteInput').val($(this).data('name'));
                $('#userAutocompleteList').empty();
            });

            $('#userAutocompleteInput').on('input', function() {
                var $el = $(this);

                $('#userAutocompleteList').empty();

                if($el.val().length >= 2) {

                    console.log($(this).val());

                    var tmpUsers = _.filter(_self.app.users.models, function(model) {
                        return model.get('name').toLowerCase().indexOf($el.val().toLowerCase()) > -1;
                    });

                    tmpUsers.forEach(function(user) {
                        $('#userAutocompleteList').append('<li class="user-form__autocomplete__item" data-name="'+ user.get('name') + '" data-id="'+ user.id + '">' +
                            '<div class="user-form__picture">' +
                            '<img src="' + user.get('avatar') + '" alt="User">' +
                            '</div>' +
                            '<div class="user-form__name">' +
                            user.get('name') +
                            '</div>' +
                            '</li>');
                    });
                }
            });

            /****************
             * Time draggable
             */

            var $timeSlideEl = this.$el.find('.time-slide');
            var timeSlideElCount = $timeSlideEl.length;

            var $timeSliderLabel = this.$el.find('#timeSliderLabel');
            var $timeSliderLabelS = $timeSliderLabel.find('#timeSliderLabelS');

            TweenMax.set($timeSliderLabelS, {autoAlpha: 0, y: 10});

            var timeSnapWidth = $timeSlideEl.first().addClass('active').outerWidth(true);

            var timeTl = new TimelineMax();

            $timeSlideEl.each(function (i) {
                timeTl.to(this, 0.5, {scale: 2, color: '#f61469', ease: Sine.easeInOut}, i - 0.5);
                timeTl.to(this, 0.5, {scale: 1, color: '#fff', ease: Sine.easeInOut}, i + 0.5);
            });

            timeTl.pause();

            timeTl.seek(0.5);
            timeTl.timeScale(5);

            var timeDraggable = Draggable.create(this.$el.find('#timeSlider'), {
                type: 'x',
                edgeResistance: 0.9,
                dragResistance: 0.1,
                throwProps: true,
                trigger: '#timeSliderWrapper',
                snap: {
                    x: function (endX) {
                        var activeElIndex = Math.max(Math.min(Math.round(endX / timeSnapWidth), 0), -timeSlideElCount + 1);
                        timeTl.seek(Math.abs(activeElIndex) + 0.5);
                        $timeSlideEl.removeClass('active');
                        $timeSlideEl.filter(':nth-child(' + Math.abs(activeElIndex - 1) + ')').addClass('active');

                        if (Math.abs(activeElIndex - 1) == 1) {
                            TweenMax.to($timeSliderLabelS[0], 0.3, {autoAlpha: 0, y: 10});
                        } else {
                            TweenMax.to($timeSliderLabelS[0], 0.3, {autoAlpha: 1, y: 0});
                        }

                        return activeElIndex * timeSnapWidth;
                    }
                },
                updateCurrentEl: function (x) {
                    timeTl.seek(-Math.max(Math.min(x / timeSnapWidth, 0), -timeSlideElCount + 1) + 0.5);
                },
                onDrag: function () {
                    this.vars.updateCurrentEl(this.x);
                },
                onDragStart: function() {
                    TweenMax.to($timeSliderLabel[0], 0.3, {opacity: 0.4, y: 7});
                },
                onDragEnd: function() {
                    TweenMax.to($timeSliderLabel[0], 0.3, {opacity: 1, y: 0});
                },
                onThrowUpdate: function () {
                    this.vars.updateCurrentEl(this.x);
                }
            });
        }

    });

    return PopupView;
});
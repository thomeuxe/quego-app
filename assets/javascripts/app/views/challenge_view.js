define(function (require, exports, module) {

    var Backbone = require('backbone');

    var Users = require('collections/users');

    var ChallengeChallengeView = require('views/challenge_challenge_view');
    var ChallengeTotemView = require('views/challenge_totem_view');

    var event_bus = require('event_bus');
    var ServerListener = require('ServerListener');
    var NotificationSender = require('NotificationSender');

    var ChallengeView = Backbone.View.extend({
        id: "pageChallengeTotem",
        tagName: "div",
        className: "innerPage page-challenge-totem",

        templateChallenge: require('text!views/templates/challenge.html'),
        templateChallengeEmpty: require('text!views/templates/challenge_empty.html'),
        templateTotem: require('text!views/templates/challenge_totem.html'),

        hammerEvents: {
            "touchend #footerTotemButton": "toggleActiveView"
        },

        initialize: function (options) {
            ServerListener.emit('start');

            this.active = "challenge";

            this.users = new Users();

            this.challengeChallengeView = new ChallengeChallengeView({model: this.model, users: this.users});
            this.challengeTotemView = new ChallengeTotemView({model: this.model});

            if (this.model.id) {
                this.initNewChallenge();
            }

            this.listenTo(this.model, 'change:id', this.initNewChallenge.bind(this));

            this.listenTo(this.model, 'change:meeting', this.initMeeting.bind(this));

            this.listenTo(this.model, 'change:id', this.initPush.bind(this));
        },

        initNewChallenge: function () {
            this.model.initSockets();

            if (this.model.get('id')) {
                this.users.url = ServerListener.serverURL + '/challenges/' + this.model.get('id') + '/users';
                this.users.fetch({reset: true});
                console.log(this.users.url);
            }
        },

        render: function () {
            if(!this.model.get('id')) {
                this.listenToOnce(this.model, 'change:id', this.render.bind(this));
                this.listenToOnce(this.model, 'change:id', function() {
                    event_bus.trigger('route:soft', 'popup/newTotem');
                    event_bus.trigger('popup:newTotem', {owner: this.model.get('owner')});
                }.bind(this));
            }
            
            this.$el.html(this.challengeChallengeView.render().el);

            if (this.model.get('id')) {
                this.$el.append(this.challengeTotemView.render().el);
                _.defer(function () {
                    this.initListeners();
                    this.scrollToActiveView();
                }.bind(this));
            }

            return this;
        },

        initListeners: function () {

            var $footerTotem = this.$el.find('#footerTotem');

            /**********
             * UI Tween
             */

            var tl = new TimelineMax({repeat: -1, yoyo: true, repeatDelay: 2, delay: 2})
                .to($footerTotem.find('.footer__totem'), 0.7, {
                    opacity: 0.8,
                    repeat: 1,
                    yoyo: true,
                    ease: Quad.easeInOut
                });

            this.btnLabel = new SplitText(this.$el.find('#btnMeetingLabel'), {type: "chars, words"});

            /**********
             * Page scroll controls
             */

            this.$draggableEl = this.$el;

            var tl = new TimelineMax({paused: true});

            tl.to($footerTotem.find('img'), 1, {autoAlpha: 0});
            tl.to($footerTotem.find('.footer__totem__arrow'), 1, {rotation: 180, force3D: true}, 0);

            this.draggable = Draggable.create(this.$draggableEl[0], {
                type: 'y',
                throwProps: true,
                maxDuration: 0.3,
                snap: {
                    y: function (endY) {

                        if (this.getDirection() == "up" && endY < -80) {
                            return -window.innerHeight;
                        } else if (this.getDirection() == "down" && endY < -window.innerHeight + 80) {
                            return -window.innerHeight;
                        } else {
                            return 0;
                        }
                    }
                },
                bounds: {
                    top: -window.innerHeight + 50,
                    height: window.innerHeight * 3 - 100,
                    left: 0,
                    width: window.innerWidth
                },
                updateTransforms: function (y) {
                    tl.seek(Math.max(-y, 0) / window.innerHeight);
                },
                onDrag: function () {
                    this.vars.updateTransforms(this.y);
                },
                onThrowUpdate: function () {
                    this.vars.updateTransforms(this.y);
                }
            });
        },

        initMeeting: function () {

            _.defer(function () {

                console.log(this.btnLabel);

                data = this.model.get('meeting');

                console.log(data);

                if (null != data) {
                    this.$el.find('#btnMeetingLink').removeAttr('href');

                    new TimelineMax()
                        .to(this.$el.find('#btnWrapperMeeting'), 0.3, {opacity: 0.4})
                        .staggerTo(this.btnLabel.chars, 0.5, {y: 10, opacity: 0, ease: Quad.easeInOut}, -0.02, 0)
                        .to(this.btnLabel.elements[0], 0.4, {width: 200})
                        .call(function () {
                            this.btnLabel.elements[0].innerHTML = data.location;
                            this.btnLabel = new SplitText(this.$el.find('#btnMeetingLabel'), {type: "chars, words"});

                            TweenMax.staggerFrom(this.btnLabel.chars, 0.5, {
                                y: 10,
                                opacity: 0,
                                ease: Quad.easeInOut
                            }, -0.02);
                        }.bind(this));
                }
            }.bind(this));

        },

        initPush: function() {
            NotificationSender.sendNewChallengePush();
        },

        toggleActiveView: function () {
            this.active = (this.active == "totem") ? "challenge" : "totem";
            this.smoothScrollToActiveView();
        },

        scrollToActiveView: function () {
            if (this.active == "totem") {
                TweenMax.set(this.draggable[0].target, {y: -window.innerHeight + 50});
                this.draggable[0].vars.updateTransforms(-window.innerHeight + 50);
            }
        },

        smoothScrollToActiveView: function () {
            var offset = (this.active == "totem") ? -window.innerHeight + 50 : 0;
            TweenMax.to(this.draggable[0].target, 0.4, {
                y: offset, ease: Quad.easeInOut, onUpdate: function () {
                    this.draggable[0].vars.updateTransforms(this.draggable[0].target._gsTransform.y);
                }.bind(this)
            });
        },

        /************
         * PAGE TRANSITIONS
         */

        enter: function (el, callback) {
            new TimelineMax({onComplete: callback, delay: 1})
                .staggerFromTo(this.$el.find('.user-bubble'), 0.4, {y: 30, opacity: 0}, {y: 0, opacity: 1}, 0.15);
        },

        exit: function (el, callback) {
            $(el).empty();
            var tEl = document.createElement("div");
            $(tEl).addClass('transition transition--simple');
            $(el).append(tEl);

            new TimelineMax()
                .staggerTo(this.$el.find('.user-bubble, .page__content__title'), 0.4, {y: -50, opacity: 0}, 0.2)
                .to(this.$el.find('#btnWrapperMeeting'), 0.5, {y: 70, opacity: 0}, "-=0.5")
                .fromTo(tEl, 0.5, {scaleY: 0, transformOrigin: '50% 60%'}, {
                    scaleY: 1,
                    transformOrigin: '50% 60%'
                }, "-=0.2")
                .call(callback)
                .fromTo(tEl, 0.5, {transformOrigin: '50% 0'}, {scaleY: 0, delay: 0.5});
        }

    });

    return ChallengeView;
});
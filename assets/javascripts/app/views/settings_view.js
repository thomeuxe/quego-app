define(function (require, exports, module) {

    var Backbone = require('backbone');

    var ScrollMagic = require('ScrollMagic');
    require('ScrollMagicGSAP');

    var _settingsData = require('json!data/settings.json');

    var SettingsView = Backbone.View.extend({
        tagName: "div",
        className: "innerPage",
        template: require('text!views/templates/settings.html'),

        hammerEvents: {
            "tap #categoryDraggable>li": "goToPanel"
        },

        initialize: function () {

        },

        render: function () {
            this.$el.html(_.template(this.template)({
                settings: _settingsData
            }));
            _.defer(function() {
                this.initListeners();
            }.bind(this));
            return this;
        },

        initListeners: function () {

            var _self = this;

            /***************
             * MATERIAL HEADER REDUCTION ON SCROLL
             */

            var controller = new ScrollMagic.Controller({container: this.$el.find('#settingsScrollContainer')[0] });

            var tl = new TimelineMax();

            var $pageParametersTitleBefore = this.$el.find('#pageParametersTitleBefore');
            var $categoryDraggable = this.$el.find('#categoryDraggable');

            TweenMax.set([$pageParametersTitleBefore[0], $categoryDraggable[0]], {z: 0, force3D: true});

            tl.to($pageParametersTitleBefore[0], 0.7, {y: -50, autoAlpha: 0, force3D: true, ease: Quad.easeIn});
            tl.to(this.$el.find('#parametersHeader')[0], 1, {y: -70, force3D: true, ease: Quad.easeInOut}, 0.1);
            tl.to($categoryDraggable[0], 0.6, {paddingTop: 8, paddingBottom: 8, force3D: true, ease: Quad.easeInOut}, 0.5);

            var scene = new ScrollMagic.Scene({
                triggerElement: this.$el.find('#scrollTrigger')[0],
                duration: 120,
                triggerHook: 0.3
            })
                .setTween(tl)
                .addTo(controller);

            /***************
             * CATEGORY SLIDER
             */

            var $categoriesEl = this.$el.find("#categoryDraggable");
            var $categoriesItems = $categoriesEl.find('li');

            console.log($($categoriesItems[0]).outerWidth() / 2);

            TweenMax.set(this.$el.find('.drag-input__handle'), {y: "-50%"});
            TweenMax.set($categoriesEl[0], {x: $(window).width() / 2 - $($categoriesItems[0]).outerWidth() / 2});
            $($categoriesItems[0]).addClass('active');

            this.prevActive = 0;

            this.categoriesDraggable = Draggable.create($categoriesEl[0], {
                type: 'x',
                throwProps: true,
                maxDuration: 0.3,
                snap: function (endX) {
                    var num = 0;
                    $categoriesItems.each(function (i, el) {
                        if (
                            Math.abs((-endX) - el.offsetLeft + $(window).width() / 2 - $($categoriesItems[0]).outerWidth() / 2) <
                            Math.abs(( -endX) - $categoriesItems[num].offsetLeft + $(window).width() / 2 - $($categoriesItems[0]).outerWidth() / 2)) {
                            num = i;
                        }
                    });

                    $categoriesItems.removeClass('active');
                    $($categoriesItems[num]).addClass('active');

                    var $prevListItem = _self.$el.find('.caracteristics__group:nth-of-type(' + (_self.prevActive + 1) + ')').find('.caracteristics__item');
                    var $currentListItem = _self.$el.find('.caracteristics__group:nth-of-type(' + (num + 1) + ')').find('.caracteristics__item');

                    if(_self.prevActive != num) {
                        var k = (_self.prevActive < num) ? 1 : -1;

                        new TimelineMax()
                            .staggerTo($prevListItem, 0.2, {x: -100 * k, opacity: 0}, 0.05)
                            .set(_self.$el.find('#caracteristics .caracteristics__group'), {x: -num * 100 + "%"})
                            .set($prevListItem, {x: 0, opacity: 1})
                            .staggerFromTo($currentListItem, 0.2, {x: 100 * k, opacity: 0}, {x: 0, opacity: 1}, 0.05);
                    }

                    _self.prevActive = num;

                    return -$categoriesItems[num].offsetLeft + $(window).width() / 2 - $($categoriesItems[num]).outerWidth() / 2;
                }
            });

            /*************
             * RANGE INPUTS
             */

            this.$el.find('.drag-input__handle').each(function (i, el) {
                var d = Draggable.create(el, {
                    type: 'x',
                    bounds: $(el).closest('.drag-input'),
                    edgeResistance: 1,
                    zIndexBoost: false,
                    onDrag: function () {
                        console.log(this);
                        TweenMax.set($(el).prev('.drag-input__active'), {scaleX: this.x / (this.maxX - this.minX)});
                    }
                });

                var r = Math.random() * (0.9 - 0.1) + 0.1;
                var rw = Math.round(r * $(el).closest('.drag-input').width());
                TweenMax.set(el, {x: rw});
                TweenMax.set($(el).siblings('.drag-input__active'), {scaleX: r + 0.01});
                d[0].update();
                d[0].drag();
            });
        },

        goToPanel: function(e) {
            console.log(this.categoriesDraggable, e.target.offsetLeft);
            var tweenOffsetX = this.categoriesDraggable[0].vars.snap(-e.target.offsetLeft + window.innerWidth/2 - $(e.target).outerWidth() / 2);
            TweenMax.to(this.categoriesDraggable[0].target, 0.3, {x: tweenOffsetX, ease: Quad.easeInOut});
        },

        /************
         * PAGE TRANSITIONS
         */

        exit: function (el, callback) {
            $(el).empty();
            var tEl = document.createElement("div");
            $(tEl).addClass('login-btn');
            $(el).append(tEl);

            TweenMax.fromTo(tEl, 0.35, {scale: 2, autoAlpha: 0}, {
                scale: 2, autoAlpha: 1, force3D: true, ease: Quad.easeOut, onComplete: function () {
                    callback();
                    TweenMax.to(tEl, 0.7, {scale: 0.2, y: window.innerHeight, force3D: true, ease: Quad.easeInOut, delay: 0.1});
                }
            });
        }

    });

    return SettingsView;
});
define(function (require, exports, module) {

    var Backbone = require('backbone');

    var LoginView = Backbone.View.extend({
        tagName: "div",
        className: "inner-content",

        template: require('text!views/templates/login.html'),

        render: function () {
            this.$el.html(this.template);

            _.defer(function () {
                this.initTween();
            }.bind(this));

            return this;
        },

        initTween: function () {
            var $btnConnexionWrapper = this.$el.find('#btnConnexionWrapper');
            TweenMax.set($btnConnexionWrapper, {
                x: "-50%",
                y: 60,
                opacity: 0,
                force3D: true
            });
            TweenMax.set(this.$el.find('.logo-appear'), {
                z: 0,
                transformOrigin: '50% 50%',
                force3D: true
            });

            this.tl = new TimelineMax({delay: 0.5, paused: true});

            this.tl.staggerFromTo(this.$el.find('.logo-appear'), 0.3, {scale: 0.6, y: 100, opacity: 0}, {
                    scale: 1,
                    y: 0,
                    opacity: 1,
                    force3D: true,
                    ease: Quad.easeOut
                }, 0.06)
                .staggerTo(this.$el.find('.logo-appear'), 0.7, {
                    y: -100,
                    force3D: true,
                    delay: 1,
                    ease: Back.easeInOut,
                    easeParams: [4]
                }, 0.03)
                .to($btnConnexionWrapper, 0.8, {
                    opacity: 1,
                    y: 0,
                    force3D: true,
                    ease: Back.easeOut
                }, '-=0.55')
                .to($btnConnexionWrapper, 1, {
                    scale: 1.1,
                    force3D: true,
                    ease: Quad.easeInOut,
                    repeat: -1,
                    delay: 0.4,
                    yoyo: true
                });
        },

        /************
         * PAGE TRANSITIONS
         */

        enter: function (el) {
            _.defer(function () {
                TweenMax.fromTo(this.$el, 1.5, {opacity: 0, z: 0, force3D: true}, {
                    opacity: 1,
                    force3D: true,
                    onComplete: function() {
                        this.tl.play();
                    }.bind(this)
                });
            }.bind(this));
        },

        exit: function (el, callback) {
            console.log(el);
            $(el).empty();
            var tEl = document.createElement("div");
            $(tEl).addClass('login-btn');
            $(el).append(tEl);

            TweenMax.set(tEl, {z: 0, force3D: true});

            TweenMax.fromTo(tEl, 0.5, {
                scale: 0, y: "50%", x: "-50%", force3D: true
            }, {
                scale: 2, force3D: true, ease: Quad.easeIn, onComplete: function () {
                    callback();

                    _.defer(function () {
                        TweenMax.to(tEl, 0.7, {y: "-150%", scale: 0.6, force3D: true, ease: Quad.easeIn, delay: 0.3});
                    }.bind(this));
                }
            });
        }

    });

    return LoginView;
});
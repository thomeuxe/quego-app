define(function (require, exports, module) {

    var Backbone = require('backbone');

    var event_bus = require('event_bus');
    var ServerListener = require('ServerListener');

    var ChallengeView = Backbone.View.extend({

        tagName: 'div',
        className: 'user-bubble',
        template: require('text!views/templates/challenge_challenger.html'),

        initialize: function (options) {
            this.listenTo(this.model, 'change:validated', this.render);
            this.listenTo(this.model, 'change:available', this.render);
        },

        render: function () {
            this.setValidated();

            if(this.model.get('validated') || !this.model.get('available')) {
                _.defer(function() {
                    this.tweenValidation();
                }.bind(this));
            }

            this.$el.html(_.template(this.template)(this.model.toJSON()));
            return this;
        },

        setValidated: function() {
            this.$el.toggleClass('validated', this.model.get('validated') || !this.model.get('available'));
        },

        tweenValidation: function() {
            var el = this.$el.find('.user-bubble__img__validation')[0];

            new TimelineMax()
                .set(el, {transformOrigin: "50% 50%"})
                .fromTo(el, 0.5, {scale: 0, opacity: 0}, {scale: 1, opacity: 0.8, force3D: true, ease: Back.easeOut})
                .set(el, {transformOrigin: "100% 0", delay: 1})
                .to(el, 0.3, {scale: 0.3, opacity: 1, force3D: true, ease: Back.easeInOut});
        }

    });

    return ChallengeView;
});
define(function (require, exports, module) {

    var Backbone = require('backbone');

    var event_bus = require('event_bus');
    var ServerListener = require('ServerListener');

    var CurrentUser = require('CurrentUser');

    var FavoriteView = require('views/favorite_view');

    var ChallengeTotemView = Backbone.View.extend({

        tagName: 'div',
        className: 'page page-totem',
        template: require('text!views/templates/challenge_totem.html'),

        initialize: function (options) {
            console.log(this.model);

            this.favoritesView = [];
        },

        render: function () {
            isFavorited = false;

            this.$el.html(_.template(this.template)({
                owner: this.model.attributes.owner,
                isFavorited: _.findIndex(CurrentUser.favorites, {'id': this.model.attributes.owner.id}) + 1
            }));

            _.defer(function() {
                this.$favoritesList = this.$el.find('#favoritesList');
                this.$favoritesWrapper = this.$favoritesList.closest('.favorites');
                this.$favoritesViewWrapper = this.$el.find('#favoriteListInner');

                this.$el.find('#favoritesToggle').on('click', this.toggleFavoritesList.bind(this));
                this.$el.find('#addTotemToFavorite').on('click', this.addTotemToFavorite.bind(this));

                this.initFavoriteList();
            }.bind(this));

            return this;
        },

        initFavoriteList: function() {
            CurrentUser.favorites.forEach(function(el) {
                var favoriteView = new FavoriteView({model: el});
                this.favoritesView.push(favoriteView);
                this.$favoritesViewWrapper.append(favoriteView.render().el);
            }.bind(this));
        },

        toggleFavoritesList: function () {
            if (!this.$favoritesWrapper.hasClass('open')) {
                this.openFavoriteList();
            } else {
                this.closeFavoriteList();
            }
        },

        openFavoriteList: function(callback) {
            callback = callback || function() {};

            var t = 0;

            if (!this.$favoritesWrapper.hasClass('open')) {
                this.$favoritesWrapper.addClass('open');

                var tmpHeight = this.$el.find('#favoritesList').height();

                TweenMax.set(this.$favoritesList[0], {
                    height: 'auto'
                });

                TweenMax.from(this.$favoritesList[0], 0.4, {
                    height: tmpHeight,
                    ease: Quad.easeInOut,
                    force3D: true
                });

                TweenMax.set(this.$favoritesList.find('.favorites__list__item'), {
                    y: 40,
                    opacity: 0
                });

                t = 0.3;
            }

            if(this.$favoritesList.find('.favorites__list__item').length) {
                TweenMax.staggerFromTo(this.$favoritesList.find('.favorites__list__item'), t, {
                    y: 40,
                    scale: 1,
                    opacity: 0
                }, {
                    y: 0,
                    scale: 1,
                    opacity: 1,
                    ease: Quad.easeOut,
                    force3D: true,
                }, t/3, callback);
            } else {
                callback();
            }
        },

        closeFavoriteList: function() {
            if (this.$favoritesWrapper.hasClass('open')) {
                this.$favoritesWrapper.removeClass('open');

                TweenMax.to(this.$favoritesList[0], 0.4, {
                    height: 0,
                    ease: Quad.easeInOut,
                    force3D: true
                });

                TweenMax.staggerTo(this.$favoritesList.find('.favorites__list__item'), 0.3, {
                    y: 40,
                    opacity: 0,
                    ease: Quad.easeOut,
                    force3D: true
                }, 0.1);
            }
        },

        addTotemToFavorite: function () {
            if(CurrentUser.addFavorite(this.model.attributes.owner)) {
                this.$el.find('#addTotemToFavorite').removeClass('btn--star-add').addClass('btn--star-added');

                var favoriteView = new FavoriteView({model: this.model.attributes.owner});

                this.openFavoriteList(function() {
                    TweenMax.to(this.$favoritesViewWrapper[0], 2, {scrollTo:{x:"max"}, ease:Power2.easeOut});
                    this.$favoritesViewWrapper.append(favoriteView.render().el);
                }.bind(this));

                this.favoritesView.push(favoriteView);
            }
        },

    });

    return ChallengeTotemView;
});
define(function (require, exports, module) {

    var Backbone = require('backbone');

    var event_bus = require('event_bus');

    var ScanView = Backbone.View.extend( {

        initialize: function() {
            navigator.VuforiaPlugin.startVuforia(
                'www/targets/Sticker-1.xml',
                [ 'sticker-01' ],
                'Scanne un sticker pour indiquer un point de rendez-vous au propriétaire d\'un totem',
                'Aetrgl//////AAAAAQNl2XJXnELRpctwwFMNmQoN+bhzkqUv92sT4eSDKOJNX4xnIgL8qv2Rxtzt2YBZPIjRBsXCBth82qDCvdS1ZzvgqYQlS4mkDQLBvjpVko/HMtYjzkK9GPbUOaqLapl7cDaAERMmw62OmkwGKSpSzgnSWaN0qItlq9G3rtcaxb5125LbuIc3BXy4QMaG9cAY0JlFfhako62MuNFigc7CV65H5j6rngYphAv+wYqxOB8UxKC4higjdgfg2JFxOnCGsi9QxilF8AQmMB7BYZb2w3C+sEgNfSfZ9dgWZzO+dn20R4XnA8Ev1+Op6/gY5BrGUhblvOtlpHQw+fd9lq/gWPZIqpV87jrj2JZFsfDc/Ere',
                this.scanSuccess,
                function(data) {
                    alert("Error: " + data);
                }
            );
        },

        scanSuccess: function () {
            TweenMax.delayedCall(1, function() {
                event_bus.trigger('popup:guessTotem');
            });
        }

    });

    return ScanView;
});
define(function (require, exports, module) {

    var Backbone = require('backbone');

    var event_bus = require('event_bus');

    var CameraInterface = require('views/interfaces/camera_interface');

    /***********
     * @extends CameraInterface
     */
    var AddPictureView = Backbone.View.extend(_.extend(CameraInterface, {

    }));

    return AddPictureView;
});
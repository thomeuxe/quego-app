define(function (require, exports, module) {

    var Backbone = require('backbone');

    var CurrentUser = require('CurrentUser');

    var ScrollMagic = require('ScrollMagic');
    require('ScrollMagicGSAP');
    require('ScrollMagicHelpers');

    var HistoryView = Backbone.View.extend({
        tagName: "div",
        className: "innerPage",

        template: require('text!views/templates/history.html'),

        hammerEvents: {
            "tap #addFavorite": "addFavorite"
        },

        initialize: function () {
            this.listenTo(this.model, 'change', this.render.bind(this));

            this.model.collection.fetch({success: function() { console.log("model fetched"); }});
        },

        render: function () {
            this.$el.html(_.template(this.template)({
                owner: this.model.attributes,
                isFavorited: _.findIndex(CurrentUser.favorites, {'id': this.model.id}) + 1,
                isCurrentUser: CurrentUser.model.id == this.model.id
            }));

            _.defer(function () {
                this.initListeners();
            }.bind(this));

            return this;
        },

        initListeners: function () {

            var tl = new TimelineMax({paused: true});

            var _self = this;

            this.anecdoteNb = _self.$el.find('.anecdote:not(.anecdote--hidden)').length;

            var $anecdotes = _self.$el.find('.anecdote');

            this.$el.find('.anecdote').each(function (i, el) {

                TweenMax.set($(el).find('.anecdote__content'), {height: 'auto', autoAlpha: 1}, 0);

                //tl.from($(el).find('.anecdote__content'), 0.3, {height: 0, autoAlpha: 0}, i);
                //tl.to($(el).find('.anecdote__content'), 0.3, {height: 0, autoAlpha: 0}, i + 1);
            });

            TweenMax.set(_self.$el.find('.anecdote__text'), {y: -30, autoAlpha: 0});
            TweenMax.set(_self.$el.find('.anecdote__text:nth-child(1)'), {y: 0, autoAlpha: 1});

            console.log(_self.$el.find('.anecdote__text:nth-child(1)'));

            _self.$el.find('.anecdote:nth-child(1)').addClass('active');

            _self.currentActive = 0;

            this.anecdoteDraggable = Draggable.create(this.$el.find('#anecdoteWrapper')[0], {
                type: 'y',
                throwProps: true,
                trigger: this.$el.find('#anecdoteContainer')[0],
                edgeResistance: 0.9,
                dragResistance: 0.3,
                maxDuration: 0.3,
                zIndexBoost: false,
                snap: {
                    y: function (endY) {
                        var tmpAnecdoteIndex = Math.min(Math.max((Math.round(-endY / 80) + 1), 1), _self.anecdoteNb);

                        _self.$el.find('.anecdote').removeClass('active');
                        _self.$el.find('.anecdote:nth-child(' + tmpAnecdoteIndex + ')').addClass('active');

                        if (_self.currentActive != tmpAnecdoteIndex - 1) {
                            _self.currentActive = tmpAnecdoteIndex - 1;

                            var k = (this.getDirection && this.getDirection() == "up") ? 1 : -1;

                            TweenMax.to(_self.$el.find('.anecdote__text'), 0.3, {
                                y: "+=" + -30 * k,
                                autoAlpha: 0,
                                force3D: true,
                                ease: Quad.easeIn,
                                onComplete: function () {
                                    TweenMax.fromTo(_self.$el.find('.anecdote__text:nth-child(' + tmpAnecdoteIndex + ')'), 0.3,
                                        {
                                            y: 30 * k,
                                            autoAlpha: 0
                                        },
                                        {
                                            y: 0,
                                            autoAlpha: 1,
                                            delay: 0.2,
                                            force3D: true,
                                            ease: Quad.easeOut
                                        });
                                }
                            });
                        } else {
                            TweenMax.to(_self.$el.find('.anecdote__text:nth-child(' + (_self.currentActive + 1) + ')'), 0.3, {
                                autoAlpha: 1,
                                y: 0
                            });
                        }

                        return Math.max(Math.min(Math.round(endY / 80) * 80, 0), -80 * (_self.anecdoteNb - 1));
                    }
                },
                onDragUpdate: function (y) {
                    var tmpBulletIndex = Math.min(Math.max(Math.round(-y / 80) + 1, 1), _self.anecdoteNb);

                    $anecdotes.removeClass('semi-active');
                    _self.$el.find('.anecdote:nth-child(' + tmpBulletIndex + ')').addClass('semi-active');
                },
                onDrag: function () {
                    this.vars.onDragUpdate(this.y);
                    /*
                     TweenMax.set(_self.$el.find('.anecdote__text:nth-child(' + (_self.currentActive + 1) + ')'), {
                     opacity: 1 - Math.abs(this.y) / 100,
                     y: this.y / 10 + _self.currentActive * 80,
                     force3D: true
                     });
                     */
                },
                onThrowUpdate: function () {
                    this.vars.onDragUpdate(this.y);
                }
            })

        },

        unlockAnecdote: function () {
            var $el = this.$el.find('.anecdote:not(.anecdote--hidden)').last().addClass('.anecdote--hidden');
            TweenMax.to(this.$el.find('#anecdoteWrapper'), 1, {y: -(this.anecdoteNb - 1) * 80});
            $el.removeClass('anecdote--hidden');
            this.anecdoteNb = this.$el.find('.anecdote:not(.anecdote--hidden)').length;

            TweenMax.delayedCall(0.5, function () {
                this.anecdoteDraggable[0].vars.onDragUpdate(-this.anecdoteNb * 80);
                this.anecdoteDraggable[0].vars.snap.y(-this.anecdoteNb * 80);
            }.bind(this));
        },

        addFavorite: function () {
            if (!_.find(CurrentUser.favorites, function (fav) {
                    return fav.id == this.model.get('owner').id;
                }.bind(this))) {
                this.$el.find('#addFavorite').removeClass('btn--star-add').addClass('btn--star-added');

                CurrentUser.favorites.push(this.model.attributes);
            }
        },

        /************
         * PAGE TRANSITIONS
         */

        exit: function (el, callback) {
            $(el).empty();
            var tEl = document.createElement("div");
            $(tEl).addClass('transition transition--simple');
            $(el).append(tEl);

            var tl = new TimelineMax({
                onComplete: function () {
                    callback();
                },
                delay: 0.2
            });

            tl.to(this.$el.find('#anecdoteContainer'), 0.3, {
                    y: 80,
                    autoAlpha: 0,
                    force3D: true,
                    ease: Quad.easeIn
                })
                .to(this.$el.find('#historyTotem'), 0.4, {
                    y: 80, autoAlpha: 0, force3D: true, ease: Quad.easeIn
                }, "-=0.2")
                .fromTo(tEl, 0.5, {scaleY: 0, transformOrigin: '50% 100%'}, {
                    scaleY: 1,
                    transformOrigin: '50% 100%'
                }, "-=0.2")
                .call(callback)
                .fromTo(tEl, 0.5, {transformOrigin: '50% 0'}, {scaleY: 0, delay: 0.5});

        }

    });

    return HistoryView;
});
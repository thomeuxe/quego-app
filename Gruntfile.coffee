path = require('path')
cordovaServer = require('./lib/cordova_server')

config = (grunt) ->
  connect:
    options:
      hostname: '0.0.0.0'
    ios:
      options:
        port: 7000
        middleware: cordovaServer.iOS
    android:
      options:
        port: 6000
        middleware: cordovaServer.android
    web:
      options:
        keepalive: true
        middleware: cordovaServer.web
        port: 5000
        livereload: true

  clean:
    default:
      options:
        "no-write": true
      src: ["!www/config.xml", "!www/js/spec/index.html", "!www/js/spec/lib/*.js", "www/*"]

  copy:
    img:
      files: [
        expand: true
        cwd: 'assets/img/'
        src: ['**/*']
        dest: 'www/img/'
      ]
    js:
      files: [
        expand: true
        cwd: 'assets/javascripts/'
        src: ['**/*.js', '**/*.html', '**/*.json']
        dest: 'www/js/'
      ]
    assets:
      files: [
        expand: true
        cwd: 'assets/public/'
        src: ['**/*', '!**/*.html']
        dest: 'www'
      ],
    recognition:
      files: [
        expand: true
        cwd: 'assets/targets/'
        src: ['**/*']
        dest: 'www/targets'
      ]

  grunticon:
    svg:
      files: [
        expand: true
        cwd: 'assets/img/icons'
        src: ['*.svg']
        dest: "assets/stylesheets/partials/icons"
      ]
      options:
        datasvgcss: '_icons.data.svg.scss'
        datapngcss: '_icons.data.png.scss'
        urlpngcss: '_icons.fallback.scss'

  less:
    compile:
      options:
        paths: ['assets/stylesheets']
      files:
        'www/css/main.css': 'assets/stylesheets/main.less'

  sass:
    dist:
      options:
        style: 'expanded'
      files:
        'www/css/main.css': 'assets/stylesheets/main.scss'

  cssmin:
    compress:
      files:
        'www/css/main.css': ['www/css/main.css']
      options:
        keepSpecialComments: 0

  requirejs:
    cordova:
      options:
        mainConfigFile: 'www/js/app/require_config.js'
        out: 'www/js/cordova.js'
        optimize: 'uglify2'
        wrap: false
        almond: true
        preserveLicenseComments: false

  htmlbuild:
    dist:
      src: ['assets/public/**/*.html']
      dest: 'www/'
      options:
        beautify: true

  mocha_phantomjs:
    options:
      reporter: 'dot'
    nerds: ['www/js/spec/index.html']

  watch:
    options:
      livereload: true
    js:
      files: ['assets/javascripts/**/*']
      tasks: ['copy:js']
    less:
      files: ['assets/stylesheets/**/*.scss']
      tasks: ['sass']
      options:
        spawn: true
    img:
      files: ['assets/img/**/*']
      tasks: ['copy:img']
    htmlbuild:
      files: ['assets/public/**/*']
      tasks: ['htmlbuild']

module.exports = (grunt) ->
  grunt.initConfig(config(grunt))

  grunt.loadNpmTasks('grunt-requirejs')
  grunt.loadNpmTasks('grunt-contrib-clean')
  grunt.loadNpmTasks('grunt-contrib-copy')
  grunt.loadNpmTasks('grunt-contrib-less')
  grunt.loadNpmTasks('grunt-contrib-sass')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-contrib-cssmin')
  grunt.loadNpmTasks('grunt-contrib-connect')
  grunt.loadNpmTasks('grunt-mocha-phantomjs')
  grunt.loadNpmTasks('grunt-html-build')
  grunt.loadNpmTasks('grunt-grunticon');

  grunt.registerTask('default', ['build'])

  grunt.registerTask('build', [
    'copy'
    'requirejs'
    'grunticon:svg'
    'sass'
    'cssmin'
    'htmlbuild'
  ])

  grunt.registerTask('server', [
    'copy:js'
    'connect:web'
  ])

  grunt.registerTask('test', [
    'mocha_phantomjs'
  ])
